from flask import Flask, send_file, jsonify
from flask import request as flask_request
import vk_audio
import vk_api
import sys

app = Flask(__name__, static_folder="static")

login, password = str(sys.argv[1]), str(sys.argv[2])
vk_session = vk_api.VkApi(login, password)
vk_session.auth()
music = vk_audio.VkAudio(vk_session)


@app.route("/")
def hello():
    return send_file('index.html')


@app.route("/get_music")
def get_music():
    uid = int(flask_request.args.get('uid'))

    data = music.load(uid)
    result = []
    for audio in data.Audios:
        if audio.url is None or audio.url == '':
            continue
        if next(filter(lambda x: x['id'] == audio.id, result), None) is not None:
            continue

        result.append({
            'id': audio.id,
            'music': audio.url,
            'artist': audio.artist,
            'title': audio.title,
        })

    return jsonify(result)


if __name__ == "__main__":
    app.run()
