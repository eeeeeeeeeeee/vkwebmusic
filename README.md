# Backend

### Project startup
```
python3 site.py vk_login vk_password
// python3 site.py 88005553535 qwerty123456
```

# Frontend

### Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Build for run on server
```
vue-cli-service build --target lib --inline-vue
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
